<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Auth extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Auth_model', 'am');
        $this->load->library('form_validation');
    }

    public function index()
    {
        // if ($this->session->userdata('email')) {
        //     redirect(user);
        // }
        $this->form_validation->set_rules('username', 'username', 'required|trim');
        $this->form_validation->set_rules('password', 'password', 'required|trim');
        $this->form_validation->set_error_delimiters('<div style="font-size: smaller;" class="text-danger mt-1">', '</div>');
        $this->form_validation->set_message('required', 'Please fill in your %s');

        if ($this->form_validation->run() == false) {
            $data['title'] = "Login Page";
            $this->load->view('../template/auth/header.php');
            $this->load->view('v_login');
            $this->load->view('../template/auth/footer.php');
        } else {
            $post = $this->input->post(null, true);
            $this->am->login($post);
        }
    }

    public function register()
    {
        $this->form_validation->set_rules('username', 'username', 'required|trim');
        $this->form_validation->set_rules('password1', 'password', 'required|trim|min_length[5]|matches[password2]', [
            'matches' => "Password dont match!",
            'min_length' => "Password too short"
        ]);
        $this->form_validation->set_rules('password2', 'password', 'required|trim|matches[password1]');
        $this->form_validation->set_rules('namaLengkap', 'full name', 'required|trim');
        $this->form_validation->set_rules('email', 'email', 'required|trim|valid_email');
        $this->form_validation->set_error_delimiters('<div style="font-size: smaller;" class="text-danger mt-1">', '</div>');
        $this->form_validation->set_message('required', 'Please fill in your %s');

        if ($this->form_validation->run() == FALSE) {
            $this->load->view('../template/auth/header.php');
            $this->load->view('v_register');
            $this->load->view('../template/auth/footer.php');
        } else {
            $post = $this->input->post(null, TRUE);
            $param = [
                'users_email' => $post['email'],
                'users_username' => $post['username'],
                'users_password' => base64_encode($post['password1']),
                'users_namaLengkap' => $post['namaLengkap'],
                'users_isActiv' => '0',
            ];

            $N = 32;
            $token = $this->get_token($N);

            $user_token = [
                'userToken_email' => $post['email'],
                'userToken_token' => $token,
                // 'userToken_created' => time()
            ];

            $this->am->do_register($param);
            $this->am->do_user_token($user_token);

            $this->_sendMail($token, 'verify');

            $this->session->set_flashdata('berhasil', '>Congratulation your account has been register');
            redirect('auth');
        }
    }

    function get_token($n)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $randomString = '';

        for ($i = 0; $i < $n; $i++) {
            $index = rand(0, strlen($characters) - 1);
            $randomString .= $characters[$index];
        }

        return $randomString;
    }

    private function _sendMail($token, $type)
    {
        $config = [
            'mailtype'  => 'html',
            'charset'   => 'utf-8',
            'protocol'  => 'smtp',
            'smtp_host' => 'ssl://smtp.gmail.com',
            'smtp_user' => 'zamrozi14@gmail.com',    // Ganti dengan email gmail kamu
            'smtp_pass' => 'zamrozi14',      // Password gmail kamu
            'smtp_port' => 465,
            'crlf'      => "\r\n",
            'newline'   => "\r\n"
        ];

        // $this->load->library('email', $config);

        $this->email->initialize($config);

        $this->email->from('zamrozi14@gmail.com', 'Zamrozi');
        $this->email->to($this->input->post('email'));

        if ($type == 'verify') {
            # code...
            $this->email->subject('Account Verification');
            $this->email->message("Click this link for <b>vericafication</b> account : <a href='" . base_url() . 'auth/verify?email=' . $this->input->post('email') . '&token=' . $token . "'>Activate!</a>");
        } else if ($type == 'forgot') {
            $this->email->subject('Password Verification');
            $this->email->message("Click this link for <b>vericafication password</b> account : <a href='" . base_url() . 'auth/forgotpass?email=' . $this->input->post('email') . '&token=' . $token . "'>Activate!</a>");
        }

        if ($this->email->send()) {
            return true;
        } else {
            echo $this->email->print_debugger();
            die();
        }
    }

    public function verify()
    {
        $email = $this->input->get('email');
        $token = $this->input->get('token');

        $user = $this->am->get_by_id_user($email)->row_array();
        if ($user) {
            $token = $this->am->get_by_id_token($token)->row_array();

            if ($token) {
                # code...
                // if (time() - $token['usersToken_created'] < (60 * 60 * 24)) {
                $this->db->set('users_isActiv', 1);
                $this->db->where('users_email', $email);
                $this->db->update('t_users');

                $this->db->delete('t_usersToken', ['userToken_email' => $email]);

                $this->session->set_flashdata('berhasil', '' . $email . ' has been activated. Please Login!');
                redirect('auth');
                // } else {
                //     $this->db->delete('t_users', ['users_email' => $email]);
                //     $this->db->delete('t_usersToken', ['usersToken_email' => $email]);

                //     $this->session->set_flashdata('pesan', 'Account Activation Failed. Token Expired!');
                //     redirect('auth');
                // }
            } else {
                $this->session->set_flashdata('pesan', 'Account Activation Failed. Wrong Token');
                redirect('auth');
            }
        } else {
            $this->session->set_flashdata('pesan', 'Account Activation Failed. Wrong Email');
            redirect('auth');
        }
    }
}
