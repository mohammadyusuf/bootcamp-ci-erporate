<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Auth_model extends CI_Model
{
    function get_by_id_token($data)
    {
        $this->db->where('userToken_token', $data);
        return $this->db->get('t_usersToken');
    }

    function get_by_id_user($data)
    {
        $this->db->where('users_email', $data);
        return $this->db->get('t_users');
    }

    public function login($post)
    {
        $user = $this->db->get_where('t_users', ['users_username' => $post['username']])->row_array();

        if ($user) {
            if ($user['users_isActiv'] == '1') {
                if (base64_encode($post['password']) == $user['users_password']) {
                    $data = [
                        'idUser' => $user['users_idUser'],
                        'user' => $user['users_username'],
                    ];
                    $this->session->set_userdata($data);
                    redirect('todo/Todoapp');
                } else {
                    $this->session->set_flashdata('pesan', 'Wrong Password');
                    redirect('auth');
                }
            } else {
                $this->session->set_flashdata('pesan', 'Email has not been activated');
                redirect('auth');
            }
        } else {
            $this->session->set_flashdata('pesan', 'Email does not register');
            redirect('auth');
        }
    }

    function do_register($data)
    {
        $this->db->insert('t_users', $data);
        return $this->db->affected_rows();
    }

    function do_user_token($data)
    {
        $this->db->insert('t_usersToken', $data);
        return $this->db->affected_rows();
    }
}
