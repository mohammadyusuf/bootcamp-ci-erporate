<div class="list-group">
    <?php foreach ($done as $data) : ?>
        <a href="#" class="list-group-item list-group-item-action flex-column align-items-start">
            <div class="d-flex w-100 justify-content-between">
                <h6 class="mb-1"><?= $data['task_name'] ?></h6>
                <small><?= waktu_lalu($data['task_createDate']) ?></small>
            </div>
            <small><?= $data['task_deskripsi'] ?></small>
            <div style="float: right">
                <?= '<a class="btn btn-sm btn-primary" href="javascript:void(0)" title="Add List" onclick="edit(' . "'" . $data['task_id'] . "'" . ')"><i class="glyphicon glyphicon-pencil"></i> Add List</a>' ?>
            </div>
        </a>
        <br>
    <?php endforeach; ?>
</div>