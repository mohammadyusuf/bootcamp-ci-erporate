<div class="list-group">
    <?php foreach ($progres as $data) : ?>
        <ul class="list-group list-group-flush">
            <li class="list-group-item">
                <b><?= $data['task_name'] ?></b>
                <div style="float: right">
                    <!-- <small class="justify-content-end"><?= waktu_lalu($data['task_createDate']) ?></small> -->
                    <?= '<a class="btn btn-sm btn-warning" href="javascript:void(0)" title="Done" onclick="done(' . "'" . $data['task_id'] . "'" . ')"><i class="glyphicon glyphicon-pencil"></i> Done</a>
            <a class="btn btn-sm btn-danger" href="javascript:void(0)" title="Delete" onclick="delete_task(' . "'" . $data['task_id'] . "'" . ')"><i class="glyphicon glyphicon-pencil"></i> Delete</a>' ?>
                </div>

            </li>
        </ul>
    <?php endforeach; ?>
</div>