<div class="list-group">
    <?php foreach ($new as $data) : ?>
        <ul class="list-group list-group-flush">
            <li class="list-group-item">
                <b><?= $data['task_name'] ?></b>
                <div style="float: right">
                    <small class="justify-content-end"><?= waktu_lalu($data['task_createDate']) ?></small>
                    <?= '<a class="btn btn-sm btn-primary" href="javascript:void(0)" title="Edit" onclick="edit(' . "'" . $data['task_id'] . "'" . ')"><i class="glyphicon glyphicon-pencil"></i> Edit</a>
            <a class="btn btn-sm btn-danger" href="javascript:void(0)" title="Delete" onclick="delete_task(' . "'" . $data['task_id'] . "'" . ')"><i class="glyphicon glyphicon-pencil"></i> Delete</a>' ?>
                </div>

            </li>
        </ul>
    <?php endforeach; ?>
</div>