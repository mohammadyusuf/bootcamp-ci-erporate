<!-- Main Content -->
<div class="main-content">
    <div class="row">
        <div class="col-12">
            <div class="card card-info">
                <div class="card-header">
                    <h4>Todo List</h4>
                    <div class="card-header-action">
                        <a href="#" onclick="tambah()" class="btn btn-info">
                            <i class="fa fa-plus"></i> Add New Task
                        </a>
                    </div>
                </div>

                <div class="card-body">
                    <div id="task_new" style="margin: 10px 20px;">
                        <?php $this->load->view('v_taskNew', array('new' => $new)); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-12">
            <div class="card card-success">
                <div class="card-header">
                    <h4>Todo Progres</h4>
                    <div class="card-header-action">
                        <a href="#" onclick="showProgres()" class="btn btn-success">
                            <i class="fa fa-plus"></i> Add Task Progres
                        </a>
                    </div>
                </div>

                <div class="card-body">
                    <div id="task_progres" style="margin: 10px 20px;">
                        <?php $this->load->view('v_taskProgres', array('progres' => $progres)); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-12">
            <div class="card card-primary">
                <div class="card-header">
                    <h4>Todo Progres</h4>
                    <!-- <div class="card-header-action">
                        <a href="#" onclick="showEnd()" class="btn btn-success">
                            <i class="fa fa-plus"></i> Add Task Progres
                        </a>
                    </div> -->
                </div>

                <div class="card-body">
                    <div id="task_end" style="margin: 10px 20px;">
                        <?php $this->load->view('v_taskEnd', array('done' => $done)); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>

<!-- Bootstrap modal -->
<div class="modal fade" id="modal_form" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title"></h3>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body form">
                <form action="#" id="form" class="form-horizontal">
                    <input type="hidden" value="" name="id" />
                    <div class="form-body">
                        <div class="form-group row">
                            <span for="nama" class="col-sm-3 col-form-label">Name</span>
                            <div class="col-sm-9">
                                <input class="form-control" type="text" name="name" />
                                <small style="font-size: 13px" class="text-danger"></small>
                            </div>
                        </div>
                        <div class="form-group row">
                            <span for="nama" class="col-sm-3 col-form-label">Due Date</span>
                            <div class="col-sm-9">
                                <input class="form-control" type="date" name="due" />
                                <small style="font-size: 13px" class="text-danger"></small>
                            </div>
                        </div>
                        <div class="form-group row">
                            <span for="nama" class="col-sm-3 col-form-label">Description</span>
                            <div class="col-sm-9">
                                <textarea class="form-control" name="desc" id="" cols="30" rows="10"></textarea>
                                <small style="font-size: 13px" class="text-danger"></small>
                            </div>
                        </div>
                    </div>
                </form>
                <div style="float: right">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                    <button type="button" class="btn btn-primary" id="btnSave" onclick="saveTask()">Simpan</button>
                </div>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- End Bootstrap modal -->

<!-- Bootstrap modal -->
<div class="modal fade" id="addProgres" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title"></h3>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body form">
                <form action="#" id="form_progres" class="form-horizontal">
                    <div class="form-body">
                        <div class="form-group row">
                            <span for="nama" class="col-sm-3 col-form-label">Name</span>
                            <div class="col-sm-9">
                                <select name="task" class="form-control" id="">
                                    <option value="">-- Please Choice --</option>
                                    <?php foreach ($new as $data) : ?>
                                        <option value="<?= $data['task_id'] ?>"><?= $data['task_name'] ?></option>
                                    <?php endforeach; ?>
                                </select>
                                <small style="font-size: 13px" class="text-danger"></small>
                            </div>
                        </div>
                    </div>
                </form>
                <div style="float: right">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                    <button type="button" class="btn btn-primary" onclick="saveProgres()" id="btn_progres">Simpan</button>
                </div>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- End Bootstrap modal -->