<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Todo_model extends CI_Model
{

    function get($where = null, $param = null)
    {
        $this->db->from('t_task');
        if ($where != null) {
            $this->db->where($where, $param);
        }
        $output = $this->db->get();
        return $output->result_array();
    }

    function get_where()
    {
        $this->db->from('t_task');
        $where = "task_status = 'done' OR task_status = 'archieve'";
        $this->db->where($where);
        $output = $this->db->get();
        return $output->result_array();
    }

    public function get_by_id($id)
    {
        $this->db->from('t_task');
        $this->db->where('task_id', $id);
        $query = $this->db->get();

        return $query->row();
    }

    public function save($data)
    {
        $this->db->insert('t_task', $data);
        return $this->db->insert_id();
    }

    public function update($where, $data)
    {
        $this->db->update('t_task', $data, $where);
        return $this->db->affected_rows();
    }

    public function delete($id)
    {
        $this->db->set('task_status', 'archieve');
        $this->db->where('task_id', $id);
        $this->db->update('t_task');
    }

    public function set_done($id)
    {
        $this->db->set('task_status', 'done');
        $this->db->where('task_id', $id);
        $this->db->update('t_task');
    }

    public function addProgres($data)
    {
        $this->db->set('task_status', 'progres');
        $this->db->where($data);
        $this->db->update('t_task');
        return $this->db->affected_rows();
    }
}
