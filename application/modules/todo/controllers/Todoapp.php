<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Todoapp extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Todo_model', 'tm');
    }

    public function index()
    {
        $data['new'] = $this->tm->get('task_status', 'new');
        $data['progres'] = $this->tm->get('task_status', 'progres');
        $data['done'] = $this->tm->get('task_status', 'done');

        $this->load->view('../template/main/header');
        $this->load->view('v_todo', $data);
        $this->load->view('../template/main/footer');
    }

    public function ajax_add()
    {
        $this->_validate();
        $post = $this->input->post(null, true);
        $data = array(
            'task_name' => $post['name'],
            'task_createDate' => date("Y-m-d H:i:s"),
            'task_dueDate' => $post['due'],
            'task_deskripsi' => $post['desc'],
            'task_status' => 'new',
        );
        $this->tm->save($data);
        $new = $this->tm->get('task_status', 'new');
        $html = $this->load->view('v_taskNew', array('new' => $new), true);
        $output = [
            'html' => $html,
            'status' => TRUE
        ];
        echo json_encode($output);
    }

    public function update()
    {
        $this->_validate();
        $post = $this->input->post(null, true);
        $data = array(
            'task_name' => $post['name'],
            'task_dueDate' => $post['due'],
            'task_deskripsi' => $post['desc'],
        );
        $this->tm->update(array('task_id' => $post['id']), $data);
        $new = $this->tm->get('task_status', 'new');
        $html = $this->load->view('v_taskNew', array('new' => $new), true);
        $output = [
            'html' => $html,
            'status' => TRUE
        ];
        echo json_encode($output);
    }

    public function ajax_edit($id)
    {
        $data = $this->tm->get_by_id($id);
        echo json_encode($data);
    }

    public function ajax_delete($id)
    {
        $this->tm->delete($id);
        $new = $this->tm->get('task_status', 'new');
        $archieve = $this->tm->get_where();
        $html = $this->load->view('v_taskNew', array('new' => $new), true);
        $done = $this->load->view('v_taskEnd', array('done' => $done), true);
        $output = [
            'html' => $html,
            'done' => $done,
            'status' => TRUE
        ];
        echo json_encode($output);
    }

    public function add_progres()
    {
        // $this->_validate();
        $post = $this->input->post(null, true);
        $data = array(
            'task_id' => $post['task'],
        );
        $this->tm->addProgres($data);
        $progres = $this->tm->get('task_status', 'progres');
        $new = $this->tm->get('task_status', 'new');
        $html = $this->load->view('v_taskProgres', array('progres' => $progres), true);
        $new = $this->load->view('v_taskNew', array('new' => $new), true);
        $output = [
            'html' => $html,
            'new' => $new,
            'status' => TRUE
        ];
        echo json_encode($output);
    }

    public function done($id)
    {
        $this->tm->set_done($id);
        $progres = $this->tm->get('task_status', 'progres');
        $done = $this->tm->get_where();
        $html = $this->load->view('v_taskProgres', array('progres' => $progres), true);
        $done = $this->load->view('v_taskEnd', array('done' => $done), true);
        $output = [
            'html' => $html,
            'done' => $done,
            'status' => TRUE
        ];
        echo json_encode($output);
    }

    private function _validate()
    {
        $data = array();
        $data['error_string'] = array();
        $data['inputerror'] = array();
        $data['status'] = TRUE;

        if ($this->input->post('name') == '') {
            $data['inputerror'][] = 'name';
            $data['error_string'][] = 'Name is required';
            $data['status'] = FALSE;
        }

        // if ($this->input->post('name_select') == '') {
        //     $data['inputerror'][] = 'name_select';
        //     $data['error_string'][] = 'Please select task';
        //     $data['status'] = FALSE;
        // }

        if ($this->input->post('due') == '') {
            $data['inputerror'][] = 'due';
            $data['error_string'][] = 'Due Date is required';
            $data['status'] = FALSE;
        }

        if ($this->input->post('desc') == '') {
            $data['inputerror'][] = 'desc';
            $data['error_string'][] = 'Description is required';
            $data['status'] = FALSE;
        }

        if ($data['status'] === FALSE) {
            echo json_encode($data);
            exit();
        }
    }
}
