var save_method = '';
var baseUrl = "http://localhost/codeigniter-hmvc/";

$(document).ready(function () {
    $("input").change(function () {
        // $(this).parent().parent().removeClass('has-error');
        $(this).next().empty();
    });
    $("select").change(function () {
        // $(this).parent().parent().removeClass('has-error');
        $(this).next().empty();
    });
})

function tambah() {
    save_method = 'add';
    $('#form')[0].reset();
    $('#modal_form').modal('show');
    $('.modal-title').text('Add New Task');
}

function showProgres() {
    $('#form_progres')[0].reset();
    $('#addProgres').modal('show');
    $('.modal-title').text('Add Task Progres');
}

function saveTask() {
    $('#btnSave').text('menyimpan...');
    $('#btnSave').attr('disabled', true);

    if (save_method == 'add') {
        url = baseUrl + 'todo/todoapp/ajax_add';
    } else {
        url = baseUrl + 'todo/todoapp/update';
    }

    $.ajax({
        url: url,
        method: 'POST',
        data: $('form#form').serialize(),
        dataType: 'JSON',
        success: function (data) {

            if (data.status) //if success close modal and reload ajax table
            {
                $('#modal_form').modal('hide');
                $('#task_new').html(data.html)
            }
            else {
                for (var i = 0; i < data.inputerror.length; i++) {
                    $('[name="' + data.inputerror[i] + '"]').next().text(data.error_string[i]);
                }
            }
            $('#btnSave').text('save'); //change button text
            $('#btnSave').attr('disabled', false); //set button enable 


        },
        error: function (jqXHR, textStatus, errorThrown) {
            alert('Error adding / update data');
            $('#btnSave').text('Save'); //change button text
            $('#btnSave').attr('disabled', false); //set button enable 

        }

    })
}

function edit(id) {
    save_method = 'update';
    $('#form')[0].reset();
    $('#btnSave').text('Update');

    //Ajax Load data from ajax
    $.ajax({
        url: baseUrl + 'todo/todoapp/ajax_edit/' + id,
        type: "GET",
        dataType: "JSON",
        success: function (data) {
            $('[name="id"]').val(data.task_id);
            $('[name="name"]').val(data.task_name);
            $('[name="due"]').val(data.task_dueDate);
            $('[name="desc"]').val(data.task_deskripsi);
            $('#modal_form').modal('show'); // show bootstrap modal when complete loaded
            $('.modal-title').text('Edit Todo List'); // Set title to Bootstrap modal title

        },
        error: function (jqXHR, textStatus, errorThrown) {
            alert('Error get data from ajax');
        }
    });
}

function delete_task(id) {
    if (confirm('Apakah anda yakin menghapus data ini?')) {

        $.ajax({
            url: baseUrl + "todo/todoapp/ajax_delete/" + id,
            type: "POST",
            dataType: "JSON",
            success: function (data) {
                //if success reload ajax table
                $('#modal_form').modal('hide');
                $('#task_new').html(data.html)
                $('#task_end').html(data.archieve)
            },
            error: function (jqXHR, textStatus, errorThrown) {
                alert('Error deleting data');
            }
        });

    }
}

function saveProgres() {
    $.ajax({
        url: baseUrl + 'todo/todoapp/add_progres',
        method: 'POST',
        data: $('form#form_progres').serialize(),
        dataType: 'JSON',
        success: function (data) {

            if (data.status) //if success close modal and reload ajax table
            {
                $('#addProgres').modal('hide');
                $('#task_progres').html(data.html)
                $('#task_new').html(data.new)
            }
            else {
                for (var i = 0; i < data.inputerror.length; i++) {
                    $('[name="' + data.inputerror[i] + '"]').next().text(data.error_string[i]);
                }
            }

        },
        error: function (jqXHR, textStatus, errorThrown) {
            alert('Error adding / update data');
        }

    })
}

function done(id) {
    //Ajax Load data from ajax
    $.ajax({
        url: baseUrl + 'todo/todoapp/done/' + id,
        type: "GET",
        dataType: "JSON",
        success: function (data) {
            if (data.status) //if success close modal and reload ajax table
            {
                $('#task_progres').html(data.html)
                $('#task_end').html(data.done)
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            alert('Error get data from ajax');
        }
    });
}